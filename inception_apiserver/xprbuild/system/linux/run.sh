#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
apt update
apt-get --assume-yes install libgl1-mesa-glx
export PYTHONPATH=${ROOT_FOLDER}
python3 ${ROOT_FOLDER}/app/main.py $@
