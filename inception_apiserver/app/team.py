from network import Network
from datetime import datetime

"""
class to re[resent a team
"""
class Team:

    """
    constructor
    Input: team ID, team name
    """
    def __init__(self, id, name):
        self.id = id
        self.name = name
        # list of members - each item is an object of type Member
        self.members = list()
        self.score = 0
        # network status for this team
        self.network = None
        self.first_login = None
        self.last_attempt = None
        self.time_taken = 0
        self.message = ""


    """
    adds a member to the team
    Input: member name, member bias
    """
    def add_member (self, name, bias):
        member = Member(name, bias)
        self.members.append(member)


    """
    sets initial network for the team
    Input: network object
    """
    def set_network (self, network:Network):
        self.network = network


    """
    merges team's network with info obtained from database regarding node status and layer biases
    """
    def merge_network(self, db_records):
        for record in db_records:
            if record["team_id"] == self.id:
                self.network.merge(record)


    """
    gets member names as comma-separated string
    """
    def get_member_names(self):
        print("Getting member names")
        member_names = list()
        for member in self.members:
            member_names.append(member.name)
        print(member_names)
        return ", ".join(member_names)


    """
    gets leaderboard entry for this team
    """
    def get_leaderboard_entry(self):
        team_record = dict()
        team_record["id"] = self.id
        team_record["name"] = self.name
        team_record["members"] = self.get_member_names()
        team_record["score"] = self.score
        team_record["time_taken"] = self.time_taken
        return team_record


    """
    updates team network based on whether recent anser was correct or not
    Input: correct_answer (boolean)
    If correct, node status is updated to complete
    If incorrect, bias of current layer is decremented
    in both cases, score is recalculated
    """
    def update (self, node_id:int, correct_answer:bool):
        self.network.update_status (node_id, correct_answer)
        self.last_attempt = datetime.now()
        self.time_taken = self.last_attempt - self.first_login

        self.score = self.network.calculate_score()

"""
class to represent a team member
"""
class Member:
    def __init__(self, name, bias):
        self.name = name
        self.bias = bias

