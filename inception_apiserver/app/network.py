"""
class to represent a neural network
The template may be a template (loaded from the parameters file of the web application), which contains the
original state of the network (only layer 0 is complete and layer 1 is ready), including correct answers
Or, may be a team-specific version, in which node statuses and layer biases reflect team activity
"""
import qr
import letter_grid
class Network:

    """
    constructor
    Input: network - network (dictionary) for initialization
    """
    def __init__(self, network:dict):
        # layers in the network - each item is an object of class Layer
        self.layers = list()
        for layer in network["layers"]:
            self.layers.append(Layer(layer))
        self.changes = list()

    """
    merges network with status and bias info from database (required for team network)
    one-time operation required when application starts up (in case of restart after a crash)
    """
    def merge (self, record: dict):
        # each db record is a dict of team_id, layer_id, bias, node_id, status
        layer = self.layers[int(record["layer_id"])]
        layer.merge(record)

    """
    finds a node with the specified ID
    Input: node ID to be found
    Return layer and node where found
    """
    def find_node (self, node_id:int):
        for layer in self.layers:
            for node in layer.nodes:
                if node.id == node_id:
                    return layer, node
        return None, None


    """
    checks answer provided by user
    Input: 
    node_id: node ID for which answer is provided
    answer: answer provided
    """
    def check_answer(self, node_id:int, answer:str):
        layer, node = self.find_node(node_id)
        return node.check_answer(answer)


    """
    updates network status based on answer
    1. If correct, mark node complete, and activate all nodes of next layer, recalculate score
    2. If incorrect, reduce bias of this layer, recalculate score
    """
    def update_status (self, node_id: int, correct_answer:bool):
        # call each layer to update_status
        layer, node = self.find_node(node_id)
        layer.update_status(node, correct_answer)
        # enable all nodes of next layer
        if correct_answer and layer.id < len(self.layers) - 1:
            self.layers[layer.id+1].enable_nodes()

        # note changes
        if correct_answer:
            self.changes.append({"type": "node_status", "node_id": node.id, "status": "complete"})
            if layer.id < len(self.layers) - 1:
                for next_node in self.layers[layer.id+1].nodes:
                    self.changes.append({"type": "node_status", "node_id": next_node.id, "status": "ready"})
        else:
            self.changes.append({"type": "layer_bias", "layer_id": layer.id, "bias": layer.bias})

    """
    remove sensitive information from all nodes
    This is required when the template network is assigned to a team, to prevent correct answers or questions for
    inactive nodes from leaking to the UI
    """
    def remove_sensitive_info(self):
        for layer in self.layers:
            layer.remove_sensitive_info()



    """
    calculate network score
    """
    def calculate_score(self):
        prev_node_scores =list()
        prev_node_scores.append(0)
        layer_scores = list()
        for layer_id in range(1, len(self.layers)):
            next_layer = self.layers[layer_id]
            prev_layer = self.layers[layer_id-1]
            next_node_scores = next_layer.calculate_score(prev_node_scores, prev_layer)
            #print(next_node_scores)
            layer_score = 0
            for node_score in next_node_scores:
                layer_score += node_score
            layer_scores.append(layer_score)
            prev_node_scores = next_node_scores
        #print(layer_scores)
        return max(layer_scores)

    """
    gets changes made due to update
    """
    def get_changes(self):
        return self.changes


    """
    resets changes
    """
    def reset_changes(self):
        self.changes = list()


"""
class to represent a layer of the network
"""
class Layer:

    """
    constructor
    Input: layer dictionary from which to initialize
    """
    def __init__(self, layer: dict):
        self.id = layer["id"]
        self.bias = layer["bias"]
        # nodes in the layer - each item is an object of class Node
        self.nodes = list()

        for node in layer["nodes"]:
            self.nodes.append(Node(node))


    """
    merges layer with status and bias info from database (required for team network)
    one-time operation required when application starts up (in case of restart after a crash)
    """
    def merge(self, record: dict):
        self.bias = record["bias"]
        for node in self.nodes:
            if node.id == record["node_id"]:
                node.merge(record)


    """
    updates network status based on answer
    1. If incorrect, reduce bias of this layer
    """
    def update_status(self, node, correct_answer:bool):
        #print("Updating status for layer {} for answer {}".format(self.id, correct_answer))
        if not correct_answer and node.status == "ready":
            #self.bias -= int(0.1*node.weight)
            self.bias -= 1
        node.update_status(correct_answer)


    """
    remove sensitive information from all nodes
    This is required when the template network is assigned to a team, to prevent correct answers or questions for
    inactive nodes from leaking to the UI
    """
    def remove_sensitive_info(self):
        for node in self.nodes:
            node.remove_sensitive_info()


    """
    calculate layer score
    """
    def calculate_score(self, prev_node_scores, prev_layer):
        node_scores = list()
        for node in self.nodes:
            node_score = 0
            if node.status == "complete":
                for i in range(len(prev_node_scores)):
                    prev_node = prev_layer.nodes[i]
                    node_score += prev_node_scores[i]*prev_node.weight
                node_score += prev_layer.bias
            node_scores.append(node_score)
        return node_scores


    """
    enables all nodes of this layer 
    (done when any one of the questions in the previous layer has been answered correctly)
    """
    def enable_nodes(self):
        for node in self.nodes:
            if node.status == "inactive":
                node.status = "ready"


"""
class too represent a node in a neural network
"""
class Node:
    def __init__(self, node:dict):
        self.id = node["id"]
        self.question = node["question"]
        self.type = node["type"]
        if self.type == "radio" or self.type == "image":
            self.options = node["options"]
        if self.type == "file":
            self.img = node["img"]
        self.weight = node["weight"]
        self.correct_answer = node["correct_answer"]
        self.status = node["status"]


    """
    merges node with status info from database (required for team network)
    one-time operation required when application starts up (in case of restart after a crash)
    """
    def merge(self, record:dict):
        self.status = record["status"]


    """
    checks whether answer is correct
    """
    def check_answer(self, answer):
        #print("Comparing {} with {}".format(answer, self.correct_answer))
        if self.type == "radio" or self.type =="text" or self.type == "image":
            if answer == self.correct_answer:
                return True
            else:
                return False
        elif self.type == "file" and self.img == "QR":
            flag = qr.decode_QR(answer, self.correct_answer)
            if flag == 1:
                print("QR code matched: ans correct")
                return True
            else:
                print("QR code did not matched: ans wrong")
                return False
        elif self.type == "file" and self.img == "Grid":
            flag = letter_grid.check_grid(answer, self.correct_answer)
            if flag == 1:
                return True
            else:
                return False
        else:
            pass


    """
    updates node status based on answer (complete if answer is correct)
    """
    def update_status(self, correct_answer):
        if correct_answer:
            if self.status == "ready":
                self.status = "complete"


    """
    remove sensitive info
    This is required when the template network is assigned to a team, to prevent correct answers or questions for
    inactive nodes from leaking to the UI
    """
    def remove_sensitive_info(self):
        self.correct_answer = ""
        if self.status == "inactive":
            self.question = ""
            self.options = list()

