from datetime import datetime, timedelta
import base64
import json

"""
class for authorization utilities
"""


class AuthUtils:
    AUTH_ERROR_MESSAGE = "Missing, invalid or expired access token"
    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

    """
    generates access token for a team
    """

    def generate_access_token(self, team_id: int):
        token = dict()
        token["team_id"] = team_id
        token["expiry"] = self.update_token_expiry_time().strftime(AuthUtils.DATE_FORMAT)
        token = self.encode_token(token)
        return token

    """
    updates token expiry time
    """

    def update_token_expiry_time(self):
        current_date_and_time = datetime.now()
        mins = 30
        mins_added = timedelta(minutes=mins)
        return current_date_and_time + mins_added


    """
    encodes access token in Base 64
    """
    def encode_token(self, token):
        token_str = json.dumps(token)
        encoded_token = token_str.encode()
        return encoded_token


    """
    decodes access token
    """
    def decode_token(self, token):
        token_bytes = token.encode()
        token_str = base64.b64decode(token_bytes)
        token = json.loads(token_str)
        return token


    """
    verifies access token
    """
    def verify_token(self, headers, team_id):
        token = headers.get("access_token")
        if token:
            token = self.decode_token(token)
            if not token["team_id"]:
                raise AuthError()
            if token["team_id"] != int(team_id):
                raise AuthError()
            if not token["expiry"]:
                raise AuthError()
            expiry_date = datetime.strptime(token["expiry"], AuthUtils.DATE_FORMAT)
            if expiry_date < datetime.now():
                raise AuthError()
        else:
            raise AuthError()

class AuthError(BaseException):

    def __init__(self, message=AuthUtils.AUTH_ERROR_MESSAGE):
        self.message = message
