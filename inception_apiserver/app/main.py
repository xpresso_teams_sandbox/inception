"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import json
import logging
from flask import Flask
from flask import request
from flask import send_from_directory
import sys
from network import Network
from team import Team
from db_utils import DBUtils
import copy
from flask_cors import CORS
import traceback
import base64
from auth_utils import AuthUtils, AuthError
import os
import random
import math

# Following import are required for Xpresso. Do not remove this.
#from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
#logger = XprLogger(name="inception_apiserver",
#                   level=logging.INFO)

parameters = dict()
teams = list()
template_network = None
db_utils = None
trivia = list()


"""
loads parameters from file
(loads template network with correct answers as well)
Input:
    parameters_file: name of parameters file (JSON)
"""


def load_parameters(parameters_file: str):
    # load parameters
    global parameters, template_network
    with open(parameters_file) as f:
        parameters = json.load(f)
        template_network = Network(parameters["network"])


"""
initializes database utilities
"""


def init_db():
    global parameters
    global db_utils
    db_utils = DBUtils(parameters["db_config"])


"""
loads teams information from database
"""


def load_teams():
    global db_utils
    global template_network
    results = db_utils.load_teams()
    curr_team_id = 0
    team = None
    for result in results:
        team_id = result["id"]
        if team_id != curr_team_id:
            team = Team(team_id, result["name"])
            team.first_login = result["first_login"]
            team.last_attempt = result["last_attempt"]
            if team.first_login and team.last_attempt:
                team.time_taken = team.last_attempt - team.first_login
            else:
                team.time_taken = 0
            teams.append(team)
        team.add_member(result["member"], result["bias"])
        curr_team_id = team_id
    for team in teams:
        team.set_network(copy.deepcopy(template_network))


"""
loads teams network status from database
"""


def load_teams_network_status():
    global db_utils, teams
    results = db_utils.load_teams_network_status()
    for team in teams:
        team.merge_network(results)
        team.score = team.network.calculate_score()


"""
loads trivia from database
"""

def load_trivia():
    global db_utils, trivia
    results = db_utils.load_trivia()
    for result in results:
        trivia.append(result["trivia"])


"""
gets random trivia
"""

def get_random_trivia():
    global trivia
    max = len(trivia)
    index = math.floor(random.uniform(0, max))
    print(index)
    if index >= max:
        index = max -1
    return trivia[index]

"""
converts object to JSON representation
"""


def get_json(obj):
    p = json.dumps(obj, default=lambda o: getattr(o, "__dict__", str(o)))
    print(p)
    return p


"""
main Flask app creation
"""


def create_app(parameters_file: str) -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    # load network, teams and initial status
    load_parameters(parameters_file)
    print(parameters["network"])
    init_db()
    load_teams()
    load_teams_network_status()
    load_trivia()

    flask_app = Flask(__name__)
    CORS(flask_app)
    image_dir = os.path.join(flask_app.instance_path, 'images')
    os.environ["image_dir"] = image_dir
    return flask_app


"""
creates a response
"""


def create_response(status, results):
    response = dict()
    response["status"] = status
    if isinstance(results, dict):
        response["results"] = results
    else:
        response["results"] = json.loads(results)
    print(response)
    return response


app = create_app(sys.argv[1])


"""
API to get team IDs and names
"""


@app.route('/teams/', methods=["POST"])
def get_teams():
    team_info = list()
    for team in teams:
        team_record = dict()
        team_record["id"] = team.id
        team_record["name"] = team.name
        team_info.append(team_record)
    team_info= sorted(team_info, key=lambda i: i["name"])

    return create_response("success", get_json(team_info))


"""
API to login a team
"""


@app.route('/team/<team_id>/login', methods=["POST"])
def login(team_id: int):
    password = request.form.get("password")
    if not team_id or not password:
        raise AuthError("Team ID or password not specified")

    team_id = int(team_id)
    params = list()
    params.append(team_id)
    params.append(password)
    results = db_utils.login_team(params)
    success = False
    for result in results:
        teams[team_id-1].first_login = result["first_login"]
        success = True
    if success:
        access_token = AuthUtils().generate_access_token(team_id)
        base64_token = base64.b64encode(access_token)
        return create_response("success", {"access_token": base64_token})
    else:
        return create_response("error", {"message": "Incorrect password or login not activated"})


"""
API to load leaderboard
"""


@app.route('/team/<team_id>/leaderboard', methods=["POST"])
def get_leaderboard(team_id: int):
    """
    Send leaderboard
    """
    global teams
    try:
        if not team_id:
            raise AuthError("Team ID not specified")

        AuthUtils().verify_token(request.headers, team_id)
        #logger.info("Processing the request")
        leaderboard = list()
        for team in teams:
            team_record = team.get_leaderboard_entry()
            leaderboard.append(team_record)
        leaderboard = sorted(leaderboard, key=lambda i: i["score"], reverse=True)
        return create_response("success", get_json(leaderboard))

    except AuthError as e:
        print ("Authorization error: {}", e.message)
        return create_response("error", {"message": e.message})

    except BaseException as e1:
        print("Error")
        traceback.print_exc()
        return create_response("error", None)


"""
API to get network status for a team
"""


@app.route('/team/<int:team_id>/', methods=["POST"])
def get_team_network(team_id, correct_answer=None):
    global teams
    # get team's network
    try:
        AuthUtils().verify_token(request.headers, team_id)
        cloned_team = copy.deepcopy(teams[team_id-1])
        cloned_team.network.remove_sensitive_info()
        cloned_team.message = ""
        if correct_answer is not None:
            if correct_answer == True:
                cloned_team.message = "Congratulations! Your answer was correct! Did you know that {}".format(get_random_trivia())
            else:
                cloned_team.message = "Oops! Incorrect answer! A penalty was applied to the bias for the current layer"
        print(get_json(cloned_team))
        return create_response("success", get_json(cloned_team))
    except AuthError as e:
        return create_response("error", {"message": e.message})
    except:
        traceback.print_exc()
        return create_response("error", {"message": "Unknown error"})


"""
API to answer a question
"""


@app.route('/team/<int:team_id>/answer_question', methods=["POST"])
def answer_question(team_id):
    # update bias and node status based on answer
    # update score
    # update database
    # return team network
    try:
        AuthUtils().verify_token(request.headers, team_id)

        node_id = request.form.get("node_id")
        print(node_id)
        if not node_id:
            raise AuthError("Node ID not specified")

        layer, node = template_network.find_node(int(node_id))
        is_grid = False
        if node.type == "text" or node.type == "radio" or node.type =="image":
            answer = request.form.get("answer")
        else:
            is_grid = (node.img == "Grid")
            answer = request.files["file"]
            # save answer to file
            uploads_dir = os.path.join(app.instance_path, 'uploads')
            os.makedirs(uploads_dir, exist_ok=True)
            filename = os.path.join(uploads_dir, str(team_id) + "_" + str(node_id))
            answer.save(filename)
            answer=filename
            print("File saved")

        if not answer:
            raise AuthError("Answer not specified")

        global teams, db_utils

        correct_answer = template_network.check_answer(int(node_id), answer)
        teams[team_id-1].update(int(node_id), correct_answer)
        # sync latest network status with database
        changes = teams[team_id - 1].network.get_changes()
        db_utils.update_team_network_changes(team_id, changes)
        teams[team_id - 1].network.reset_changes()
        return get_team_network(team_id, correct_answer)

    except AuthError as e:
        return create_response("error", {"message": e.message})
    except:
        traceback.print_exc()
        return create_response("error", {"message": "Unknown error"})



"""
get help
"""
@app.route('/help')
def get_help():
    return send_from_directory(app.instance_path, 'help.html')


"""
get images
"""
@app.route('/images/<path:filename>')
def get_image(filename):
    return send_from_directory(os.path.join(app.instance_path, 'images'), filename)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        #logger.info("Usage: inception_apiserver <parameter_file_name>")
        exit(-1)

    app.run(host="0.0.0.0", threaded=True)
