import cv2
def decode_QR(filename,original_data):
    try:
        img = cv2.imread(filename)
        detector = cv2.QRCodeDetector()
        data, bbox, straight_qrcode = detector.detectAndDecode(img)
        print(data)
        if data.lower().strip() == original_data.lower():
            return 1
        else:
            return 0
    except Exception as error:
        print(error)

