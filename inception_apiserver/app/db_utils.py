import pymysql
import traceback
"""
class for database utilities
"""
class DBUtils:

    SELECT_QUERY = "select"
    UPDATE_QUERY = "update"

    """
    initializes database utilities with config info
    """
    def __init__(self, db_params: dict):
        self.host = db_params["host"]
        self.user = db_params["user"]
        self.password=db_params["password"]
        self.port = 3306
        self.database = "inception"
        self.charset = "utf8mb4"
        self.cursorclass = pymysql.cursors.DictCursor



    """
    connects to database
    """
    def connect(self):
        try:
            connection = pymysql.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password,
                database=self.database,
                charset=self.charset,
                cursorclass=self.cursorclass
            )
            return connection
        except:
            print("Database connection failed")



    """
    executes a stored procedure
    Input: sp_name name of tored procedure
    kwargs: any parameters to the SP
    """
    def execute_sp(self, sp_name:str, type=SELECT_QUERY, params:list=None):
        if type == DBUtils.SELECT_QUERY:
            self.cursorclass = pymysql.cursors.DictCursor
        else:
            self.cursorclass = pymysql.cursors.Cursor
        connection = self.connect()
        cursor = connection.cursor()
        #stmt = "call {}".format(sp_name)
        results = None
        #print(params)
        if not params:
            params = list()
        # if params:
        #     stmt += "("
        #     for i in range (0, len(params) - 1):
        #         param = params[i]
        #         stmt += str(param) + ", "
        #     stmt += str(params[len(params)-1])
        #     stmt += ")"
        # print (stmt)
        # cursor.execute(stmt)
        try:
            params = tuple(params)
            print("Calling SP {} with params {}".format(sp_name, params))
            cursor.callproc(sp_name, params)
            connection.commit()
            results = cursor.fetchall()
            print("Successfully executed procedure")
        except:
            traceback.print_exc()

        cursor.close()
        #connection.next_result()
        connection.close()
        return results


    """
    logs in a team
    """
    def login_team(self, login_info:list):
        return self.execute_sp("login", DBUtils.SELECT_QUERY, login_info)

    """
    loads team information from database
    """
    def load_teams (self):
        return self.execute_sp("get_teams")



    """
    loads team network status from database
    """
    def load_teams_network_status(self):
        return self.execute_sp("get_teams_network_status")


    """
    loads trivia from database
    """
    def load_trivia(self):
        return self.execute_sp("get_trivia")


    """
    updates layer bias of team
    Input: change - dict containing team_id, layer_id and bias
    """
    def update_team_layer_bias (self, change):
        return self.execute_sp("update_team_layer_bias", DBUtils.UPDATE_QUERY, change)

    """
    updates node status of team
    Input: change - dict containing team_id, node_id and status
    """
    def update_team_node_status (self, change):
        return self.execute_sp("update_team_node_status", DBUtils.UPDATE_QUERY, change)

    """
    updates team network changes
    Input: team_id team ID
    changes: changes to be updated
    """
    def update_team_network_changes(self, team_id, changes:list):
        for change in changes:
            print(change)
            params = list()
            params.append(team_id)
            if change["type"] == "layer_bias":
                params.append(change["layer_id"])
                params.append(change["bias"])
                #print(params)
                self.update_team_layer_bias(params)
            else:
                params.append(change["node_id"])
                params.append(change["status"])
                #print(params)
                self.update_team_node_status(params)
