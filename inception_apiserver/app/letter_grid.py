import cv2
import os

def check_grid(answer_filename, correct_filename):
    try:
        image_dir = os.environ["image_dir"]
        correct_filename = os.path.join(image_dir, correct_filename)
        print("Comparing {} with {}".format(answer_filename, correct_filename))

        img_rgb = cv2.imread(answer_filename)
        img_rgb = cv2.resize(img_rgb, (128, 128))
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

        template = cv2.imread(correct_filename, 0)
        template = cv2.resize(template, (128, 128))

        res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
        threshold = 0.88
        if res >= threshold:
            print("Letter grid matched, Ans correct, res = {}".format(res))
            return 1
        else:
            print("Letter grid did not match, Ans wrong, res = {}".format(res))
            return 0
    except Exception as error:
        print(error)




