CREATE DATABASE  IF NOT EXISTS `inception`;
USE `inception`;

-- MySQL dump 10.13  Distrib 5.7.20, for Win64 (x86_64)
--
-- Host: localhost    Database: inception
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Team 1'),(2,'Team 2');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams_layers`
--

DROP TABLE IF EXISTS `teams_layers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `layer_id` int(11) DEFAULT NULL,
  `bias` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `teams_layers_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams_layers`
--

LOCK TABLES `teams_layers` WRITE;
/*!40000 ALTER TABLE `teams_layers` DISABLE KEYS */;
INSERT INTO `teams_layers` VALUES (1,1,0,15),(2,1,1,-6),(3,1,2,0),(4,1,3,0),(5,1,4,0),(6,1,5,0),(7,2,0,40),(8,2,1,0),(9,2,2,0),(10,2,3,0),(11,2,4,0),(12,2,5,0);
/*!40000 ALTER TABLE `teams_layers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams_members`
--

DROP TABLE IF EXISTS `teams_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `member` varchar(100) DEFAULT NULL,
  `bias` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `teams_members_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams_members`
--

LOCK TABLES `teams_members` WRITE;
/*!40000 ALTER TABLE `teams_members` DISABLE KEYS */;
INSERT INTO `teams_members` VALUES (1,1,'A',10),(2,1,'B',20),(3,2,'C',30),(4,2,'D',40),(5,2,'E',50);
/*!40000 ALTER TABLE `teams_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams_nodes`
--

DROP TABLE IF EXISTS `teams_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `layer_id` int(11) DEFAULT NULL,
  `node_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `teams_nodes_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams_nodes`
--

LOCK TABLES `teams_nodes` WRITE;
/*!40000 ALTER TABLE `teams_nodes` DISABLE KEYS */;
INSERT INTO `teams_nodes` VALUES (1,1,0,0,'complete'),(2,1,1,1,'complete'),(3,1,1,2,'ready'),(4,1,1,3,'ready'),(5,1,2,4,'ready'),(6,1,2,5,'ready'),(7,1,3,6,'inactive'),(8,1,3,7,'inactive'),(9,1,4,10,'inactive'),(10,1,4,12,'inactive'),(11,1,5,14,'inactive'),(12,1,6,15,'inactive'),(13,1,3,8,'inactive'),(14,1,3,9,'inactive'),(15,1,4,11,'inactive'),(16,1,5,13,'inactive'),(17,2,0,0,'complete'),(18,2,1,1,'ready'),(19,2,1,2,'ready'),(20,2,1,3,'ready'),(21,2,2,4,'inactive'),(22,2,2,5,'inactive'),(23,2,3,6,'inactive'),(24,2,3,7,'inactive'),(25,2,3,8,'inactive'),(26,2,3,9,'inactive'),(27,2,4,10,'inactive'),(28,2,4,11,'inactive'),(29,2,4,12,'inactive'),(30,2,5,13,'inactive'),(31,2,5,14,'inactive'),(32,2,6,15,'inactive');
/*!40000 ALTER TABLE `teams_nodes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-29 10:07:07

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_teams`()
BEGIN
	SELECT t.id, t.name, tm.member, tm.bias
    FROM teams t inner join teams_members tm
    ON t.id = tm.team_id;
END $$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_teams_network_status`()
BEGIN
	SELECT tl.team_id, tl.layer_id, tl.bias, tn.node_id, tn.status
    from teams_layers tl inner join teams_nodes tn
    ON tl.team_id = tn.team_id and tl.layer_id = tn.layer_id;
END $$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_team_layer_bias`(IN team_id INT, IN layer_id INT, IN bias INT)
BEGIN
	UPDATE teams_layers tl set tl.bias = bias where tl.team_id = team_id and tl.layer_id = layer_id;
END $$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_team_node_status`(team_id INT, node_id INT, status varchar(20))
BEGIN
	UPDATE teams_nodes tn set tn.status = status where tn.team_id = team_id and tn.node_id = node_id;
END $$
DELIMITER ;
